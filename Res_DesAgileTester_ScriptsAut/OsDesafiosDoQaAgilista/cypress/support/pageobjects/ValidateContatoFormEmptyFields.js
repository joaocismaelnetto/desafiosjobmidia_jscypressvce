/// <reference types="Cypress" />

import ValidateContatoFormEmptyFieldsElements from 
'../elements/ValidateContatoFormEmptyFieldsElements'
const validateContatoFormEmptyFieldsElements = new ValidateContatoFormEmptyFieldsElements
//const url = Cypress.env("baseUrl")

class ValidateContatoFormEmptyFields {
    // Acessa o site job midia que será testado
    acessarSite() {
        cy.visit(Cypress.env('baseUrl'))
    }

    // verifica se o botão contato está visivel
    botaoContatoVisivel() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoContato()).should('contain', 'Contato')
    }

    // verifica se o botão contato está disponivel para seleção
    botaoContatoHabilitado() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoContato()).should('not.be.disabled')
    }

    // Clica no botão contato que acessa o formulario contato
    clicarBotaoContato() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoContato()).click()
    }
      
    // Verifica se titulo do formulario é texto "Titulo"
   visualizarTituloContato() {
    cy.get(validateContatoFormEmptyFieldsElements.tituloContato()).should('contain', 'Contato')
    }

    
 // Verifica se cabecalho nome do formulario é texto "Nome"
 visualizarCabecalhoNome() {
    cy.get(validateContatoFormEmptyFieldsElements.nomeCabecalhoContato()).should('contain', 'Nome')
    }

    // Verifica se nome formulario está disponivel para edicao
   visualizarNome() {
    cy.get(validateContatoFormEmptyFieldsElements.nomeContato()).should('not.be.disabled')
    }

    // verifica se msg preenchimento nome obrigatorio está exibida
    visualizarMensagemNome() {
    cy.get(validateContatoFormEmptyFieldsElements.nomeMensagemContato()).should('contain', 'Por favor informe seu nome.')
    }

    // Verifica se cabecalho email do formulario é texto "Email"
    visualizarCabecalhoEmail() {
    cy.get(validateContatoFormEmptyFieldsElements.emailCabecalhoContato()).should('contain', 'Email')
    }

    // Verifica se email formulario está disponivel para edicao
   visualizarEmail() {
    cy.get(validateContatoFormEmptyFieldsElements.emailContato()).should('not.be.disabled')
    }

    // verifica se msg preenchimento email obrigatorio está exibida
     visualizarMensagemEmail() {
    cy.get(validateContatoFormEmptyFieldsElements.emailMensagemContato()).should('contain', 'Por favor informe seu e-mail.')
        }

    
    // Verifica se cabecalho telefone do formulario é texto "telefone"
    visualizarCabecalhoTelefone() {
    cy.get(validateContatoFormEmptyFieldsElements.telefoneCabecalhoContato()).should('contain', 'Telefone')
        }
    
    // Verifica se telefone formulario está disponivel para edicao
    visualizarTelefone() {
    cy.get(validateContatoFormEmptyFieldsElements.telefoneContato()).should('not.be.disabled')
        }
    
    // verifica se msg preenchimento telefone obrigatorio está exibida
    visualizarMensagemTelefone() {
    cy.get(validateContatoFormEmptyFieldsElements.telefoneMensagemContato()).should('contain', 'Por favor informe seu telefone.')
            }


    // Verifica se cabecalho mensagem do formulario é texto "mensagem"
    visualizarCabecalhoMensagem() {
    cy.get(validateContatoFormEmptyFieldsElements.mensagemCabecalhoContato()).should('contain', 'Mensagem')
        }
    
    // Verifica se mensagem formulario está disponivel para edicao
    visualizarMensagem() {
    cy.get(validateContatoFormEmptyFieldsElements.mensagemContato()).should('not.be.disabled')
        }
    
    // verifica se msg preenchimento mensagem obrigatorio está exibida
   visualizarMensagemMensagem() {
    cy.get(validateContatoFormEmptyFieldsElements.mensagemMensagemContato()).should('contain', 'Por favor informe uma mensagem.')
            }


    // verifica se o botão enviar está visivel
     botaoEnviarVisivel() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoEnviar()).should('contain', 'Enviar')
    }

    // verifica se o botão enviar está disponivel para seleção
    botaoEnviarHabilitado() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoEnviar()).should('not.be.disabled')
    }

    // Clica no botão Enviar
    clicarBotaoEnviar() {
        cy.get(validateContatoFormEmptyFieldsElements.botaoEnviar()).click()
    }
}

export default ValidateContatoFormEmptyFields;
