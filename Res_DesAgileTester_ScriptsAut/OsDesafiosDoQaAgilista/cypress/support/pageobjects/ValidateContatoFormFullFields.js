/// <reference types="Cypress" />

import ValidateContatoFormFullFieldsElements from 
'../elements/ValidateContatoFormFullFieldsElements'
const validateContatoFormFullFieldsElements = new ValidateContatoFormFullFieldsElements
//const url = Cypress.env("baseUrl")

class ValidateContatoFormFullFields {
    // Acessa o site job midia que será testado
    acessarSite() {
        cy.visit(Cypress.env('baseUrl'))
    }

    // verifica se o botão contato está visivel
    botaoContatoVisivel() {
        cy.get(validateContatoFormFullFieldsElements.botaoContato()).should('contain', 'Contato')
    }

    // verifica se o botão contato está disponivel para seleção
    botaoContatoHabilitado() {
        cy.get(validateContatoFormFullFieldsElements.botaoContato()).should('not.be.disabled')
    }

    // Clica no botão contato que acessa o formulario contato
    clicarBotaoContato() {
        cy.get(validateContatoFormFullFieldsElements.botaoContato()).click()
    }
      
    // Verifica se titulo do formulario é texto "Titulo"
    visualizarTituloContato() {
    cy.get(validateContatoFormFullFieldsElements.tituloContato()).should('contain', 'Contato')
    }

       // Verifica se cabecalho nome do formulario é texto "Nome"
   visualizarCabecalhoNome() {
    cy.get(validateContatoFormFullFieldsElements.nomeCabecalhoContato()).should('contain', 'Nome')
    }

    // Verifica se nome formulario está disponivel para edicao
   visualizarNome() {
    cy.get(validateContatoFormFullFieldsElements.nomeContato()).should('not.be.disabled')
    }

    // Verifica se nome formulario está vazio
   visualizarNome() {
    cy.get(validateContatoFormFullFieldsElements.nomeContato()).should('be.empty')
    }

    // Preenche nome
   visualizarNome() {
    cy.get(validateContatoFormFullFieldsElements.nomeContato()).type('Joao Carlos Ismael Netto')
    }


    // Verifica se cabecalho email do formulario é texto "Email"
    visualizarCabecalhoEmail() {
    cy.get(validateContatoFormFullFieldsElements.emailCabecalhoContato()).should('contain', 'Email')
    }

    // Verifica se email formulario está disponivel para edicao
   visualizarEmail() {
    cy.get(validateContatoFormFullFieldsElements.emailContato()).should('not.be.disabled')
    }

    // Verifica se email formulario está vazio
   visualizarEmail() {
    cy.get(validateContatoFormFullFieldsElements.emailContato()).should('be.empty')
    }

    // Preenche nome
    visualizarEmail() {
    cy.get(validateContatoFormFullFieldsElements.emailContato()).type('joaocismael@hotmail.com')
    }

    
    // Verifica se cabecalho telefone do formulario é texto "telefone"
    visualizarCabecalhoTelefone() {
    cy.get(validateContatoFormFullFieldsElements.telefoneCabecalhoContato()).should('contain', 'Telefone')
        }
    
    // Verifica se telefone formulario está disponivel para edicao
    visualizarTelefone() {
    cy.get(validateContatoFormFullFieldsElements.telefoneContato()).should('not.be.disabled')
        }
    
    // Verifica se telefone formulario está vazio
    visualizarTelefone() {
    cy.get(validateContatoFormFullFieldsElements.telefoneContato()).should('be.empty')
        }
    
    // Preenche telefone
   visualizarTelefone() {
    cy.get(validateContatoFormFullFieldsElements.telefoneContato()).type('11998040955')
    }

 
    
    // Verifica se cabecalho mensagem do formulario é texto "mensagem"
    visualizarCabecalhoMensagem() {
    cy.get(validateContatoFormFullFieldsElements.mensagemCabecalhoContato()).should('contain', 'Mensagem')
        }
    
    // Verifica se mensagem formulario está disponivel para edicao
    visualizarMensagem() {
    cy.get(validateContatoFormFullFieldsElements.mensagemContato()).should('not.be.disabled')
        }
    
    // Verifica se mensagem formulario está vazio
    visualizarMensagem() {
    cy.get(validateContatoFormFullFieldsElements.mensagemContato()).should('be.empty')
        }
    
    // Preenche mensagem
   visualizarMensagem() {
    cy.get(validateContatoFormFullFieldsElements.mensagemContato()).type('Os desafios do Qa agilista')
    }    
   
   

    // verifica se o botão enviar está visivel
     botaoEnviarVisivel() {
        cy.get(validateContatoFormFullFieldsElements.botaoEnviar()).should('contain', 'Enviar')
    }

    // verifica se o botão enviar está disponivel para seleção
    botaoEnviarHabilitado() {
        cy.get(validateContatoFormFullFieldsElements.botaoEnviar()).should('not.be.disabled')
    }

    // Clica no botão Enviar
    clicarBotaoEnviar() {
        cy.get(validateContatoFormFullFieldsElements.botaoEnviar()).click()
    }

    // verifica se msg envio com sucesso está exibida
    visualizarMensagemSucesso() {
        cy.get(validateContatoFormFullFieldsElements.sucessoMensagemContato()).should('contain', 'Sua mensagem foi enviada com sucesso. ')
                }    
}

export default ValidateContatoFormFullFields;