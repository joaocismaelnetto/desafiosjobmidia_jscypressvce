/// <reference types="Cypress" />

import ValidateContatoButtonElements from 
'../elements/ValidateContatoButtonElements'
const validateContatoButtonElements = new ValidateContatoButtonElements
//const url = Cypress.env("baseUrl")

class ValidateContatoButton {
    // Acessa o site job midia que será testado
    acessarSite() {
        cy.visit(Cypress.env('baseUrl'))
    }

    // verifica se o botão contato está visivel
    botaoContatoVisivel() {
        cy.get(validateContatoButtonElements.botaoContato()).should('contain', 'Contato')
    }

    // verifica se o botão contato está disponivel para seleção
    botaoContatoHabilitado() {
        cy.get(validateContatoButtonElements.botaoContato()).should('not.be.disabled')
    }

    // Clica no botão contato que acessa o formulario contato
    clicarBotaoContato() {
        cy.get(validateContatoButtonElements.botaoContato()).click()
    }
      
    // Verifica se titulo do formulario é texto "Titulo"
   visualizarTituloContato() {
    cy.get(validateContatoButtonElements.tituloContato()).should('contain', 'Contato')
    }

   // Verifica se cabecalho nome do formulario é texto "Nome"
   visualizarCabecalhoNome() {
    cy.get(validateContatoButtonElements.nomeCabecalhoContato()).should('contain', 'Nome')
    }

    // Verifica se nome formulario está disponivel para edicao
   visualizarNome() {
    cy.get(validateContatoButtonElements.nomeContato()).should('not.be.disabled')
    }

    // Verifica se nome formulario está vazio
   visualizarNome() {
    cy.get(validateContatoButtonElements.nomeContato()).should('be.empty')
    }

    // Clica no nome
   visualizarNome() {
    cy.get(validateContatoButtonElements.nomeContato()).click()
    }

    // Verifica se cabecalho email do formulario é texto "Email"
    visualizarCabecalhoEmail() {
    cy.get(validateContatoButtonElements.emailCabecalhoContato()).should('contain', 'Email')
    }

    // Verifica se email formulario está disponivel para edicao
   visualizarEmail() {
    cy.get(validateContatoButtonElements.emailContato()).should('not.be.disabled')
    }

    // Verifica se email formulario está vazio
   visualizarEmail() {
    cy.get(validateContatoButtonElements.emailContato()).should('be.empty')
    }

    // Clica no email
   visualizarEmail() {
    cy.get(validateContatoButtonElements.emailContato()).click()
    }

    
    // Verifica se cabecalho telefone do formulario é texto "telefone"
    visualizarCabecalhoTelefone() {
    cy.get(validateContatoButtonElements.telefoneCabecalhoContato()).should('contain', 'Telefone')
        }
    
    // Verifica se telefone formulario está disponivel para edicao
    visualizarTelefone() {
    cy.get(validateContatoButtonElements.telefoneContato()).should('not.be.disabled')
        }
    
    // Verifica se telefone formulario está vazio
    visualizarTelefone() {
    cy.get(validateContatoButtonElements.telefoneContato()).should('be.empty')
        }
    
    // Clica no telefone
    visualizarTelefone() {
    cy.get(validateContatoButtonElements.telefoneContato()).click()
        }
        
    
    // Verifica se cabecalho mensagem do formulario é texto "mensagem"
    visualizarCabecalhoMensagem() {
    cy.get(validateContatoButtonElements.mensagemCabecalhoContato()).should('contain', 'Mensagem')
        }
    
    // Verifica se mensagem formulario está disponivel para edicao
    visualizarMensagem() {
    cy.get(validateContatoButtonElements.mensagemContato()).should('not.be.disabled')
        }
    
    // Verifica se mensagem formulario está vazio
    visualizarMensagem() {
    cy.get(validateContatoButtonElements.mensagemContato()).should('be.empty')
        }
    
    // Clica no mensagem
    visualizarMensagem() {
    cy.get(validateContatoButtonElements.mensagemContato()).click()
        }


    // verifica se o botão enviar está visivel
     botaoEnviarVisivel() {
        cy.get(validateContatoButtonElements.botaoEnviar()).should('contain', 'Enviar')
    }

    // verifica se o botão enviar está disponivel para seleção
    botaoEnviarHabilitado() {
        cy.get(validateContatoButtonElements.botaoEnviar()).should('not.be.disabled')
    }
}

export default ValidateContatoButton;
