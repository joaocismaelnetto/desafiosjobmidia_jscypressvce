/* global Given, Then, When */

import ValidateContatoFormFullFields from
 '../pageobjects/ValidateContatoFormFullFields'
const validateContatoFormFullFields = new ValidateContatoFormFullFields

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

// Acesso o site job midia
Given("acesso o site jobmidia", () => {
    validateContatoFormFullFields.acessarSite();
})
// Tratar botão Contato

Given("visualizar o botao contato visivel", () => {
    validateContatoFormFullFields.botaoContatoVisivel();
})

Given("visualizar o botao contato habilitado", () => {
    validateContatoFormFullFields.botaoContatoHabilitado();
})


Given("clicar o botao contato", () => {
    validateContatoFormFullFields.clicarBotaoContato();
})
// Tratar titulo contato
Given("visualizar o titulo contato", () => {
    validateContatoFormFullFields.visualizarTituloContato();
})

// Tratar nome
Given("visualizar o cabecalho nome", () => {
    validateContatoFormFullFields.visualizarCabecalhoNome();
})

Given("visualizar o nome habilitado", () => {
    validateContatoFormFullFields.visualizarNome();
})

Given("visualizar o nome vazio", () => {
    validateContatoFormFullFields.visualizarNome();
})

Given("preencher o nome", () => {
    validateContatoFormFullFields.visualizarNome();
})


// Tratar email
Given("visualizar o cabecalho email", () => {
    validateContatoFormFullFields.visualizarCabecalhoEmail();
})

Given("visualizar o email habilitado", () => {
    validateContatoFormFullFields.visualizarEmail();
})

Given("visualizar o email vazio", () => {
    validateContatoFormFullFields.visualizarEmail();
})

Given("preencher o email", () => {
    validateContatoFormFullFields.visualizarEmail();
})




// Tratar telefone
Given("visualizar o cabecalho telefone", () => {
    validateContatoFormFullFields.visualizarCabecalhoTelefone();
})

Given("visualizar o telefone habilitado", () => {
    validateContatoFormFullFields.visualizarTelefone();
})

Given("visualizar o telefone vazio", () => {
    validateContatoFormFullFields.visualizarTelefone();
})

Given("preencher o telefone", () => {
    validateContatoFormFullFields.visualizarTelefone();
})



// Tratar mensagem
Given("visualizar o cabecalho mensagem", () => {
    validateContatoFormFullFields.visualizarCabecalhoMensagem();
})

Given("visualizar o mensagem habilitado", () => {
    validateContatoFormFullFields.visualizarMensagem();
})

Given("visualizar o mensagem vazio", () => {
    validateContatoFormFullFields.visualizarMensagem();
})

Given("preencher a mensagem", () => {
    validateContatoFormFullFields.visualizarMensagem();
})


// Tratar botão Enviar
Given("visualizar o botao enviar visivel", () => {
    validateContatoFormFullFields.botaoEnviarVisivel();
})

Given("visualizar o botao enviar habilitado", () => {
    validateContatoFormFullFields.botaoEnviarHabilitado();
})

When("clico no botao Enviar", () => {
    validateContatoFormFullFields.clicarBotaoEnviar();
})

// Tratar mensagem de sucesso de envio formulario
Then("devo visualizar a msg envio com sucesso", () => {
    validateContatoFormFullFields.visualizarMensagemSucesso();
})

// Tratar botão Enviar
Then("devo visualizar o botao enviar visivel", () => {
    validateContatoFormEmptyFields.botaoEnviarVisivel();
})

Then("devo visualizar o botao enviar habilitado", () => {
    validateContatoFormEmptyFields.botaoEnviarHabilitado();
})

