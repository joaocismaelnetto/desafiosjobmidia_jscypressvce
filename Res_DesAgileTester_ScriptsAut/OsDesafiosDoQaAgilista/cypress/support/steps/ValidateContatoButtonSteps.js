/* global Given, Then, When */

import ValidateContatoButton from
 '../pageobjects/ValidateContatoButton'
const validateContatoButton = new ValidateContatoButton

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

Given("acesso o site jobmidia", () => {
    validateContatoButton.acessarSite();
})
// Tratar botão Contato

Given("visualizar o botao contato visivel", () => {
    validateContatoButton.botaoContatoVisivel();
})

Given("visualizar o botao contato habilitado", () => {
    validateContatoButton.botaoContatoHabilitado();
})


When("clico no botao contato", () => {
    validateContatoButton.clicarBotaoContato();
})

// Tratar titulo contato
Then("devo visualizar o titulo contato", () => {
    validateContatoButton.visualizarTituloContato();
})

// Tratar nome
Then("devo visualizar o cabecalho nome", () => {
    validateContatoButton.visualizarCabecalhoNome();
})

Then("devo visualizar o nome habilitado", () => {
    validateContatoButton.visualizarNome();
})

Then("devo visualizar o nome vazio", () => {
    validateContatoButton.visualizarNome();
})

Then("devo clicar no nome", () => {
    validateContatoButton.visualizarNome();
})

// Tratar email
Then("devo visualizar o cabecalho email", () => {
    validateContatoButton.visualizarCabecalhoEmail();
})

Then("devo visualizar o email habilitado", () => {
    validateContatoButton.visualizarEmail();
})

Then("devo visualizar o email vazio", () => {
    validateContatoButton.visualizarEmail();
})

Then("devo clicar no email", () => {
    validateContatoButton.visualizarEmail();
})

// Tratar telefone
Then("devo visualizar o cabecalho telefone", () => {
    validateContatoButton.visualizarCabecalhoTelefone();
})

Then("devo visualizar o telefone habilitado", () => {
    validateContatoButton.visualizarTelefone();
})

Then("devo visualizar o telefone vazio", () => {
    validateContatoButton.visualizarTelefone();
})

Then("devo clicar no telefone", () => {
    validateContatoButton.visualizarTelefone();
})

// Tratar mensagem
Then("devo visualizar o cabecalho mensagem", () => {
    validateContatoButton.visualizarCabecalhoMensagem();
})

Then("devo visualizar o mensagem habilitado", () => {
    validateContatoButton.visualizarMensagem();
})

Then("devo visualizar o mensagem vazio", () => {
    validateContatoButton.visualizarMensagem();
})

Then("devo clicar no mensagem", () => {
    validateContatoButton.visualizarMensagem();
})

// Tratar botão Enviar
Then("devo visualizar o botao enviar visivel", () => {
    validateContatoButton.botaoEnviarVisivel();
})

Then("devo visualizar o botao enviar habilitado", () => {
    validateContatoButton.botaoEnviarHabilitado();
})
