/* global Given, Then, When */

import ValidateContatoFormEmptyFields from
 '../pageobjects/ValidateContatoFormEmptyFields'
const validateContatoFormEmptyFields = new ValidateContatoFormEmptyFields

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

Given("acesso o site jobmidia", () => {
    validateContatoFormEmptyFields.acessarSite();
})
// Tratar botão Contato

Given("visualizar o botao contato visivel", () => {
    validateContatoFormEmptyFields.botaoContatoVisivel();
})

Given("visualizar o botao contato habilitado", () => {
    validateContatoFormEmptyFields.botaoContatoHabilitado();
})

// Tratar titulo contato
Given("visualizar o titulo contato", () => {
    validateContatoFormEmptyFields.visualizarTituloContato();
})

// Tratar nome
Given("visualizar o cabecalho nome", () => {
    validateContatoFormEmptyFields.visualizarCabecalhoNome();
})

Given("visualizar o nome habilitado", () => {
    validateContatoFormEmptyFields.visualizarNome();
})

Given("visualizar o nome vazio", () => {
    validateContatoFormEmptyFields.visualizarNome();
})

Given("clicar no nome", () => {
    validateContatoFormEmptyFields.visualizarNome();
})

// Tratar email
Given("visualizar o cabecalho email", () => {
    validateContatoFormEmptyFields.visualizarCabecalhoEmail();
})

Given("visualizar o email habilitado", () => {
    validateContatoFormEmptyFields.visualizarEmail();
})

Given("visualizar o email vazio", () => {
    validateContatoFormEmptyFields.visualizarEmail();
})

Given("clicar no email", () => {
    validateContatoFormEmptyFields.visualizarEmail();
})

// Tratar telefone
Given("visualizar o cabecalho telefone", () => {
    validateContatoFormEmptyFields.visualizarCabecalhoTelefone();
})

Given("visualizar o telefone habilitado", () => {
    validateContatoFormEmptyFields.visualizarTelefone();
})

Given("visualizar o telefone vazio", () => {
    validateContatoFormEmptyFields.visualizarTelefone();
})

Given("clicar no telefone", () => {
    validateContatoFormEmptyFields.visualizarTelefone();
})

// Tratar mensagem
Given("visualizar o cabecalho mensagem", () => {
    validateContatoFormEmptyFields.visualizarCabecalhoMensagem();
})

Given("visualizar o mensagem habilitado", () => {
    validateContatoFormEmptyFields.visualizarMensagem();
})

Given("visualizar o mensagem vazio", () => {
    validateContatoFormEmptyFields.visualizarMensagem();
})

Given("clicar no mensagem", () => {
    validateContatoFormEmptyFields.visualizarMensagem();
})


// Tratar botão Enviar
Given("visualizar o botao enviar visivel", () => {
    validateContatoFormEmptyFields.botaoEnviarVisivel();
})

Given("visualizar o botao enviar habilitado", () => {
    validateContatoFormEmptyFields.botaoEnviarHabilitado();
})

When("clico no botao Enviar", () => {
    validateContatoFormEmptyFields.clicarBotaoEnviar();
})

// Tratar nome
Then("devo visualizar a msg preencher nome", () => {
    validateContatoFormEmptyFields.visualizarMensagemNome();
})

// Tratar email
Then("devo visualizar a msg preencher email", () => {
    validateContatoFormEmptyFields.visualizarMensagemEmail();
})

// Tratar telefone
Then("devo visualizar a msg preencher telefone", () => {
    validateContatoFormEmptyFields.visualizarMensagemTelefone();
})

// Tratar mensagem
Then("devo visualizar a msg preencher mensagem", () => {
    validateContatoFormEmptyFields.visualizarMensagemMensagem();
})

// Tratar botão Enviar
Then("devo visualizar o botao enviar visivel", () => {
    validateContatoFormEmptyFields.botaoEnviarVisivel();
})

Then("devo visualizar o botao enviar habilitado", () => {
    validateContatoFormEmptyFields.botaoEnviarHabilitado();
})
