class ValidateContatoFormFullFieldsElements {
    botaoContato = () => { return '#contatomenu > a' }
    
    tituloContato = () => { return '#contact > .container > :nth-child(1) > .col-lg-12 > h2' }
    
     
    nomeCabecalhoContato = () => { return '#Name > label' }

    nomeContato = () => { return '#name' }


    emailCabecalhoContato = () => { return '#Email > label' }
    emailContato = () => { return '#email' }
    
    
    telefoneCabecalhoContato = () => { return '#Phone > label' }
    telefoneContato = () => { return '#phone' }
    
    
    mensagemCabecalhoContato = () => { return '#Message > label' }
    mensagemContato = () => { return '#message' }
    

    sucessoMensagemContato = () => { return '#success > div > strong' } 


    botaoEnviar = () => { return '#enviarbotao' }
    
    tituloEnviar = () => { return '#contactForm > div:nth-child(7) > div' }    
    
}

export default ValidateContatoFormFullFieldsElements;