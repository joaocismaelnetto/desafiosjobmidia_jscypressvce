class ValidateContatoFormEmptyFieldsElements {
    botaoContato = () => { return '#contatomenu > a' }
    
    tituloContato = () => { return '#contact > .container > :nth-child(1) > .col-lg-12 > h2' }
    
    nomeCabecalhoContato = () => { return '#Name > label' }
    nomeContato = () => { return '#name' }    
    nomeMensagemContato = () => { return '#Name > .help-block > ul > li' }

    
    emailCabecalhoContato = () => { return '#Email > label' }
    emailContato = () => { return '#email' }
    emailMensagemContato = () => { return '#Email > p > ul > li' }    

    
    telefoneCabecalhoContato = () => { return '#Phone > label' }
    telefoneContato = () => { return '#phone' }
    telefoneMensagemContato = () => { return '#Phone > p > ul > li' }    
    

    mensagemCabecalhoContato = () => { return '#Message > label' }
    mensagemContato = () => { return '#message' }
    mensagemMensagemContato = () => { return '#Message > p > ul > li' } 


    botaoEnviar = () => { return '#enviarbotao' }
    tituloEnviar = () => { return '#contactForm > div:nth-child(7) > div' }   
    
}

export default ValidateContatoFormEmptyFieldsElements;