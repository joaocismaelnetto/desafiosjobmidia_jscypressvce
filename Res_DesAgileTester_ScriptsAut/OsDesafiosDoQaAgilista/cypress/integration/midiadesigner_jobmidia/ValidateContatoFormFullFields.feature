Feature: validar preenchimento completo e correto do formulario contato

    Scenario: Validar o comportamento do formulario Contato para todos os campo preenchido validos 
        Given acesso o site jobmidia
        
        Given visualizar o botao contato visivel
        Given visualizar o botao contato habilitado            
        Given clicar o botao contato

        Given visualizar o titulo contato
        
        Given visualizar o cabecalho nome 
        Given visualizar o nome habilitado
        Given visualizar o nome vazio
        Given preencher o nome

        Given visualizar o cabecalho email 
        Given visualizar o email habilitado
        Given visualizar o email vazio
        Given preencher o email
        
        Given visualizar o cabecalho telefone 
        Given visualizar o telefone habilitado
        Given visualizar o telefone vazio
        Given clicar no telefone
        Given preencher o telefone
       
        Given visualizar o cabecalho mensagem 
        Given visualizar o mensagem habilitado
        Given visualizar o mensagem vazio
        Given preencher a mensagem

        Given visualizar o botao enviar visivel
        Given visualizar o botao enviar habilitado

        When clico no botao Enviar
        
        Then devo visualizar a msg envio com sucesso

        Then devo visualizar o botao enviar visivel
        Then devo visualizar o botao enviar habilitado


 