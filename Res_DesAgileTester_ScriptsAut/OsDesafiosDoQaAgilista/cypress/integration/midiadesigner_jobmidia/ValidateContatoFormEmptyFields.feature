Feature: validar preenchimento obrigatorio do formulario contato

    Scenario: Validar o comportamento do formulario Contato para nenhum campo preenchido 
        Given acesso o site jobmidia
        
        Given visualizar o botao contato visivel
        Given visualizar o botao contato habilitado            
        
        Given visualizar o titulo contato
        
        Given visualizar o cabecalho nome 
        Given visualizar o nome habilitado
        Given visualizar o nome vazio
        Given clicar no nome
        
        Given visualizar o cabecalho email 
        Given visualizar o email habilitado
        Given visualizar o email vazio
        Given clicar no email

        Given visualizar o cabecalho telefone 
        Given visualizar o telefone habilitado
        Given visualizar o telefone vazio
        Given clicar no telefone
       
        Given visualizar o cabecalho mensagem 
        Given visualizar o mensagem habilitado
        Given visualizar o mensagem vazio
        Given clicar no mensagem

        Given visualizar o botao enviar visivel
        Given visualizar o botao enviar habilitado

        When clico no botao Enviar

        
        Then devo visualizar a msg preencher nome
        
        Then devo visualizar a msg preencher email

        Then devo visualizar a msg preencher telefone
       
        Then devo visualizar a msg preencher mensagem

        Then devo visualizar o botao enviar visivel
        Then devo visualizar o botao enviar habilitado