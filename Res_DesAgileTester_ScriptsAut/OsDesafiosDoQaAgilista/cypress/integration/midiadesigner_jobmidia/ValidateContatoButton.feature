Feature: validar acesso ao formulario contato

    Scenario: Validar o comportamento do botão Contato
        Given acesso o site jobmidia
        
        Given visualizar o botao contato visivel
        Given visualizar o botao contato habilitado            
        
        When clico no botao contato
        
        Then devo visualizar o titulo contato
        
        Then devo visualizar o cabecalho nome 
        Then devo visualizar o nome habilitado
        Then devo visualizar o nome vazio
        Then devo clicar no nome
        
        Then devo visualizar o cabecalho email 
        Then devo visualizar o email habilitado
        Then devo visualizar o email vazio
        Then devo clicar no email

        Then devo visualizar o cabecalho telefone 
        Then devo visualizar o telefone habilitado
        Then devo visualizar o telefone vazio
        Then devo clicar no telefone
       
        Then devo visualizar o cabecalho mensagem 
        Then devo visualizar o mensagem habilitado
        Then devo visualizar o mensagem vazio
        Then devo clicar no mensagem

        Then devo visualizar o botao enviar visivel
        Then devo visualizar o botao enviar habilitado

        