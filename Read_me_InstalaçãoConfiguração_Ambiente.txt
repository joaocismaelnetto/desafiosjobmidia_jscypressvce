Instruções para download, instalação e configuração Desafio Qa agilista (Node/Visual code/Cypress/Cucumber)
===========================================================================================================

Obs.: 	1. Base de conhecimento de instalação/configuração cypress (ferramenta automação end to end)link abaixo: 
https://medium.com/cwi-software/testes-automatizados-com-cypress-e-cucumber-d78b211da766

	2. Os programas de infraestrutura de automação também estão copiados na pasta: InstalaçãoConfiguraçãoAmbienteAutomação


1. Instalar Node.js: acesse o site oficial, baixe e instale a versão nodejs para seu computador.
https://nodejs.org/en/download/

2. Instalar visual code: acesse o site oficial, baixe e instale a versão cisual code para seu computador.
https://code.visualstudio.com/download

3. Baixe, do google drive, convite enviado via email, o aquivo "JoaoIsmaeNetto_ResoluçãoDesafioMidiaDesigner" na pasta documentos
https://drive.google.com/drive/folders/1MGaGbDdsnQoRQx1tN_9WNApJ_7L2zbe8?usp=sharing

4. Entrar na pasta Resolução_Desafio_AgileTester_ScriptsAutomação e copiar o caminho

5. Entrar na pasta Resolução_Desafio_AgileTester_ScriptsAutomação via linha de comando windowsn (cmd)

6. Instalar o cypress cucumber via linha de comando Windows dentro da pasta JoaoIsmaeNetto_ResoluçãoDesafioMidiaDesigner para instalar o cucumber 
npm install --save-dev cypress cypress-cucumber-preprocessor

ou 

6.Instalar Cypress: acesse o site oficial Cypress, role a pagina 
ate Direct download e clique no link "dowload cypress directly from our CDN", realize o download 
https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements

	6.1.Execute o arquivo cypress downloaded dentro da pasta JoaoIsmaeNetto_ResoluçãoDesafioMidiaDesigner

 	6.2.Instalar o cypress cucumber via linha de comando Windows dentro da pasta JoaoIsmaeNetto_ResoluçãoDesafioMidiaDesigner para instalar o cucumber 
npm install --save-dev cypress cypress-cucumber-preprocessor


7.Abrir o visual code e selecione file/open folder\Documents\JoaoIsmaeNetto_ResoluçãoDesafioMidiaDesigner\Resolução_Desafio_AgileTester_ScriptsAutomação\OsDesafiosDoQaAgilista


8.Selecione no visual code terminal/new terminal na barra de comando

9.Na linha de comando digite: npx cypress open

10. Com o painel de comando cypress aberto clique nos cenários/scripts de automação em ValidateContatoButton, ValidateContatoFormEmptyField e ValidateContatoFormFullFields
para executar os scripts de automação.

11.Quando concluir a execução da automação clique em sair e selecione outro cenários/scripts


